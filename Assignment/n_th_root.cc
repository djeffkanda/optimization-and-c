/*
D'jeff Kanda, 20/12/2018
Introduction to C++
n_th root
*/
#include<iostream>
#include<vector>




double power_iter_eff(double q,int n){
    /*
    This function compute  q to the power n by using a loop.
    Involving the fact that q^n=   (q^2)^(n/2) when n is even
    */
    double p=1.0;
    while(n>0){
        if(n%2==0){
            q*=q;
            n=n/2;
        }
        p*=q;
        n--;   
    }
    return p; 
}


double root_iterative(double q, int n,int steps){
    double a=1;
    for (int i=0;i<steps;i++){
        a=a+1.0/n*(q/power_iter_eff(a,n-1)-a);
    }
    return a;
}

void test_root(double q,int n,int steps){
    double a=root_iterative(q,n,steps);
    double p=power_iter_eff(a,n);
    std::cout<<"q="<<q<<" n="<<n<<" a="<<a<<" a^n="<<p<<" q-a^n="<<q-p<<std::endl;
    return;
}

int main(int argc, char const *argv[])
{
    double q;
    int n;
    int steps;

    std::cout<<"Input q :"<<std::endl;
    std::cin>>q;

    if(std::cin.fail() || q<0){
        std::cout<<"Wrong value entered"<<std::endl;
        return 0;
    }

    std::cout<<"Input n :"<<std::endl;
    std::cin>>n;
    
    if(std::cin.fail() || n<0){
        std::cout<<"Wrong value entered"<<std::endl;
        return 0;
    }

    std::cout<<"Input steps:"<<std::endl;
    std::cin>>steps;
    
    if(std::cin.fail() || steps<0){
        std::cout<<"Wrong value entered"<<std::endl;
        return 0;
    }
    test_root(q,n,steps);
}