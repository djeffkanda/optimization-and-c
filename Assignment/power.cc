/*
D'jeff Kanda, 20/12/2018
Introduction to C++
Optimization of the power
*/
#include<iostream>



int pow_iterative(int q,int n){

    /*
    This function compute  q to the power n by using a loop
    */
    int p=1;
    while(n>0){
     p*=q;
     n--;   
    }
    return p; 
}

int pow_recursive(int q,int n){
    /*
    This function compute  q to the power n by using a recursion.
    */
    if (n==0)
        return 1;
    else
        return q*pow_recursive(q,n-1);
}

int power_iter_eff(int q,int n){
    /*
    This function compute  q to the power n by using a loop.
    Involving the fact that q^n=   (q^2)^(n/2) when n is even
    */
    int p=1;
    while(n>0){
        if(n%2==0){
            q*=q;
            n=n/2;
        }
        p*=q;
        n--;   
    }
    return p; 
}

int power_rec_eff(int q,int n){
    /*
    This function compute  q to the power n by using recursion.
    Involving the fact that q^n=   (q^2)^(n/2) when n is even
    */
    if (n==0)
        return 1;
    else{
        if (n%2==0)
            return power_rec_eff(q*q,n/2);
        return q*power_rec_eff(q,n-1);
    }
}

int main(int argc, char const *argv[])
{
    int q;
    int n;

    std::cout<<"Input q :"<<std::endl;
    std::cin>>q;

    if(std::cin.fail()){
        std::cout<<"Wrong value entered"<<std::endl;
        return 0;
    }

    std::cout<<"Input n :"<<std::endl;
    std::cin>>n;
    
    if(std::cin.fail() || n<0){
        std::cout<<"Wrong value entered"<<std::endl;
        return 0;
    }
    
    std::cout<<"power iterative :"<<pow_iterative(q,n)<<std::endl;
    std::cout<<"power recursive :"<<pow_recursive(q,n)<<std::endl;
    std::cout<<"power iterative efficient :"<<power_iter_eff(q,n)<<std::endl;
    std::cout<<"power recursive efficient ::"<<power_rec_eff(q,n)<<std::endl;
    return 0;
}
