/*
D'jeff Kanda, 20/12/2018
Introduction to C++
n_th root
*/
#include<iostream>
#include<array>
#include<vector>
#include<algorithm>

std::array<double,2> read_point(){
    std::array<double,2> point;
    std::cin>>point[0]>>point[1];
    return point;
}

std::vector<std::array<double,2> > read_problem(){
    int n;
    std::cin>>n;
    std::vector<std::array<double,2> > v(n);
    for(int i=0;i<n;i++){
        v[i]=read_point();
    }
    return v;
}

double distSq(std::array<double,2> p1, std::array<double,2> p2) 
{ 
    return (p1[0] - p2[0])*(p1[0] - p2[0]) + 
          (p1[1]- p2[1])*(p1[1] - p2[1]); 
}

int orientation(std::array<double,2>  a, std::array<double,2>  b, std::array<double,2>  c) 
{ 
    int val = (b[0] - a[0]) * (c[1] - a[1]) - 
              (c[0] - a[0]) * (b[1] - a[1]); 
  
    if (val == 0) return 0;  // is on AB
    return (val < 0)? 1: 2; // right or left AB
}



// Find the lowest y-coordinate point
int find_lowest_y_p(std::vector<std::array<double,2> > v){ 
   double ymin = v[0][1];
   int min = 0; 
   for (int i = 1; i < v.size(); i++) 
   { 
     double y = v[i][1]; 
     // Pick the the lowest y-coordinate with lowest x coordinate 
     if ((y < ymin) || (ymin == y && 
         v[i][0] < v[min][0])) 
        ymin = v[i][1], min = i; 
   } 
   return min;
}
std::array<double,2> p0;
// Angle comparator
bool compare(std::array<double,2> p1, std::array<double,2> p2) 
{ 
    
   int o = orientation(p0, p1, p2); 
   if (o == 0) 
     return (distSq(p0,p2 ) >= distSq(p0, p1))? false : true; 
  
   return (o == 2)? false: true; 
} 

int main(int argc, char const *argv[])
{
   std::vector<std::array<double,2> > v=read_problem();
   for(int i = 0; i < v.size(); i++)
   {
       std::cout<<v[i][0]<<v[i][1]<<std::endl;
   }

    int min_indice=find_lowest_y_p(v);
    // swap points[0] with the point with the lowest y-coordinate
    std::swap(v[0], v[min_indice]); 
    //sort points by polar angle with points[0]
    // using function as comp
    p0=v[0];

    std::sort (v.begin()+1, v.end(), compare);
    

    std::cout<<"Sorted "<<std::endl;
    for(int i = 0; i < v.size(); i++)
   {
       std::cout<<v[i][0]<<v[i][1]<<std::endl;
   }

   // If two or more points make same angle with p0, 
   // Remove all but the one that is farthest from p0 
   // Remember that, in above sorting, our criteria was 
   // to keep the farthest point at the end when more than 
   // one points have same angle. 
   int m = 1; // Initialize size of modified array 
   for (int i=1; i<v.size(); i++) 
   { 
       // Keep removing i while angle of i and i+1 is same 
       // with respect to p0 
       while (i < v.size()-1 && orientation(p0, v[i], v[i+1]) == 0) 
          i++; 
       v[m] = v[i]; 
       m++;  // Update size of modified array 
   } 

    return 0;
    
}