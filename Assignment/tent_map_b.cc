/*
D'jeff Kanda, 20/12/2018
Introduction to C++
Tent map 
*/
#include<iostream>

float ftmap(float x){
    if (x>=0 && x<0.5)
        return 1.999999*x;
    else
        return 1.999999*(1-x);
}

int main(int argc, char const *argv[])
{
    float x=0.01401;
    std::cout<<x<<std::endl;
    for (int i=1;i<=100;i++){
        x=ftmap(x);
        std::cout<<x<<std::endl;
    }
    return 0;
}