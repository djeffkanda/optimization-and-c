/*
D'jeff Kanda, 20/12/2018
Introduction to C++
convex hull using Graham_scan
Reference for the algorithm : https://en.wikipedia.org/wiki/Graham_scan
*/
#include<iostream>
#include<array>
#include<vector>
#include<algorithm>


//(a) and (b) Function read point
std::array<double,2> read_point(){
    std::array<double,2> point;
    std::cin>>point[0]>>point[1];
    return point;
}

//(c) Function Read problem from file
std::vector<std::array<double,2> > read_problem(){
    int n;
    std::cin>>n;
    std::vector<std::array<double,2> > v(n);
    for(int i=0;i<n;i++){
        v[i]=read_point();
    }
    return v;
}

//Determine the position of C
int orientation_(std::array<double,2>  a, std::array<double,2>  b, std::array<double,2>  c) 
{ 
    double val = (b[0] - a[0]) * (c[1] - a[1]) - 
              (c[0] - a[0]) * (b[1] - a[1]); 
  
    if (val == 0) return 0;  // is on AB
    return (val < 0)? 1: 2; // 1 right or 2 left AB
}

void printvect (std::vector<std::array<double,2> > P){
     for(int i = 0; i < P.size(); i++)
   {
        std::cout <<P[i][0]<<" "<<P[i][1]<< std::endl;
   }
    return;
}
// points comparator for sorting
bool comparelexic(std::array<double,2> p1, std::array<double,2> p2) 
{ 
    return p1[0]<p2[0] || (p1[0]==p2[0] && p1[1]<p2[1]);
} 

// (d) Convex hull calculation
std::vector<std::array<double,2> > convex_hull(std::vector<std::array<double,2> > P)
{
    if (P.size()<3) return P;
    
    std::sort (P.begin(), P.end(), comparelexic);
    std::vector<std::array<double,2> > v_hull;
 
    for(int i=2;i<P.size();i++){
        while(v_hull.size()>=2 && orientation_(v_hull[v_hull.size()-2],v_hull[v_hull.size()-1], P[i])!=2 ){
            v_hull.pop_back();
        }
        v_hull.push_back(P[i]);
    }
    
  
    for(int i=P.size()-1,t=v_hull.size()+1;i>0;--i){
        while(v_hull.size()>=t && orientation_(v_hull[v_hull.size()-2],v_hull[v_hull.size()-1], P[i-1])!=2 ){
            v_hull.pop_back();
        }
        v_hull.push_back(P[i-1]);
    }
    return v_hull;
}

int main(int argc, char const *argv[])
{
    std::vector<std::array<double,2> > v=read_problem();
    std::vector<std::array<double,2> > v2=v;
    std::vector<std::array<double,2> > v_hull=convex_hull(v);
    
    //(e) Finding indices of points on convex hull
    for(int i = 0; i < v_hull.size(); i++)
   {
        std::vector<std::array<double,2> >::iterator match = std::find(v2.begin(), v2.end(), v_hull[i]);    
        if(match != v2.end()) {
            std::cout << (match - v2.begin()) << std::endl;
        } else {
            std::cout << "not found" << std::endl;
        }
        
   }
   return 0;    
}