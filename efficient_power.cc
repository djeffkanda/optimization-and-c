/*
D'jeff Kanda, 11/12/2018
Introduction to C++
Optimization of the power
*/
#include<iostream>



double iter_power(int q,int n){

    /*
    This function compute the q to the power n by using a loop
    */
    int p=1;
    while(n>0){
     p*=q;
     n--;   
    }
    return p; 
}

double iter_eff_power(int q,int n){
    /*
    This function compute the q to the power n by using a loop.
    Involving the fact that q^n=   (q^2)^(n/2) when n is even
    */
    int p=1;
    while(n>0){
        if(n%2==0){
            q*=q;
            n=n/2;
        }
        p*=q;
        n--;   
    }
    return p; 
}

double rec_power(int q,int n){
    /*
    This function compute the q to the power n by using a recursion.
    */
    if (n==0)
        return 1;
    else
        return q*rec_power(q,n-1);
}

double rec_eff_power(int q,int n){
    /*
    This function compute the q to the power n by using recursion.
    Involving the fact that q^n=   (q^2)^(n/2) when n is even
    */
    if (n==0)
        return 1;
    else{
        if (n%2==0)
            return rec_power(q*q,n/2);
        return q*rec_power(q,n-1);
    }
}
void print_time(std::string txt,std::chrono::steady_clock::time_point begin,std::chrono::steady_clock::time_point end){
    std::cout << "Time difference "+txt+" = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() <<std::endl;
    std::cout << "Time difference "+txt+" = "  << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() <<std::endl<<std::endl;
    
}


int main(int argc, char const *argv[])
{
    int q;
    int n;

    std::cout<<"Input q :"<<std::endl;
    std::cin>>q;
    std::cout<<"Input n :"<<std::endl;
    std::cin>>n;

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    double p=iter_power(q,n);
    std::cout<<"result="<<p<<std::endl;
    std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();

    print_time("iter_power",begin,end);

    begin = std::chrono::steady_clock::now();
    p=rec_power(q,n);
    std::cout<<"result="<<p<<std::endl;
    end= std::chrono::steady_clock::now();
    
    print_time("rec_power",begin,end);


    begin = std::chrono::steady_clock::now();
    p=rec_eff_power(q,n);
    std::cout<<"result="<<p<<std::endl;
    end= std::chrono::steady_clock::now();

    print_time("rec_eff_power",begin,end);

    begin = std::chrono::steady_clock::now();
    p=iter_eff_power(q,n);
    std::cout<<"result="<<p<<std::endl;
    end= std::chrono::steady_clock::now();

    print_time("iter_eff_power",begin,end);    

    return 0;
}
