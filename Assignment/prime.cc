/*
D'jeff Kanda, 20/12/2018
Primality test
*/
#include<iostream>


bool isPrime(int n){
    if (n<=3)
     return n>1;
    else if (n%2==0 || n%3==0)
        return false;
    int i=5;
    while(i*i<=n){
        if (n%i==0 || n%(i+2)==0)
            return false;
        i+=6;
    } 
   return true;
}

void printPrime(int upto){
    for(int i=2;i<=upto;i++)
        if(isPrime(i))
            std::cout<<i<<std::endl;
    return;
}

int main(int argc, char const *argv[])
{
    int upto;
    std::cout<<"Enter the value of upto for primes  :";
    std::cin >>upto;

    while(upto<2){
        std::cout<<"Enter the value of upto for primes >1  :";
        std::cin >>upto;
    }
    printPrime(upto);
    return 0;
}
