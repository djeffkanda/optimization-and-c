/*
D'jeff Kanda, 11/12/2018
Introduction to C++
*/
#include<iostream>

int main(int argc, char const *argv[])
{
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    std::cout<<"Hello World"<<std::endl;
    std::chrono::steady_clock::time_point end= std::chrono::steady_clock::now();
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() <<std::endl;
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count() <<std::endl;
    return 0;
}
