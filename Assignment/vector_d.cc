/*
D'jeff Kanda, 20/12/2018
Introduction to C++
Vector
*/
#include<iostream>
#include<vector>
#include<cmath>

void printvector(std::vector<double>v){
    
    for(int i = 0; i < v.size(); i++)
    {
        std::cout<<v[i]<<"   ";
    }
    std::cout<<std::endl;
}

int main(int argc, char const *argv[])
{
    int n;
    std::cout<<"Enter the size of the vector : ";
    std::cin>>n;
    if(n<1 || std::cin.fail())
     {
      std::cout<<"Wrong value entered "<<std::endl;
      return 0;   
     }
    std::vector<double> v(n);

    for(int i=0;i<n;i++){
         std::cout<<"Enter V["<<i+1<<"]: ";
         std::cin>>v[i];
        if(std::cin.fail())
        {
        std::cout<<"Wrong value entered "<<std::endl;
        return 0;   
        }
    }

    //Reverse in the same vector
    std::cout<<"Original vector : "<<std::endl;
    printvector(v);
    int temp;
    for(int i=(v.size()-1)/2;i>=0;i--){
        temp=v[i];
        v[i]=v[v.size()-1-i];
        v[v.size()-1-i]=temp;
    }
    std::cout<<"Reversed vector : "<<std::endl;
    printvector(v);

}