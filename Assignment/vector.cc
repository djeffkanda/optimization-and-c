/*
D'jeff Kanda, 20/12/2018
Introduction to C++
Vector
*/
#include<iostream>
#include<vector>

void find_min_max(std::vector<int>v){
    int max=v[0];
    int min=v[0];

    for(int i=1; i<v.size();i++)
    {
        if(v[i]>max)
                max=v[i];
        if (v[i]<min)
                min=v[i];
    }
    std::cout<<"Max="<<max<<std::endl<<"Min="<<min<<std::endl;
    return;
}

std::vector<int> reverse(std::vector<int>v){
    std::vector<int>vrvs;
    for(int i=v.size()-1;i>=0;i--){
        vrvs.push_back(v[i]);
    }
    return vrvs;
}

void printvector(std::vector<int>v){
    
    for(int i = 0; i < v.size(); i++)
    {
        std::cout<<v[i]<<"   ";
    }
    std::cout<<std::endl;
}

int main(int argc, char const *argv[])
{
    int n;
    std::cout<<"Enter the size of the vector : ";
    std::cin>>n;
    if(n<1 || std::cin.fail())
     {
      std::cout<<"Wrong value entered "<<std::endl;
      return 0;   
     }
     std::vector<int> v(n);

    for(int i=0;i<n;i++){
         std::cout<<"Enter V["<<i+1<<"]: ";
         std::cin>>v[i];
        if(std::cin.fail())
        {
        std::cout<<"Wrong value entered "<<std::endl;
        return 0;   
        }
     }
    find_min_max(v);
    std::cout<<"Original vector : "<<std::endl;
    printvector(v);
    std::cout<<"Reversed vector : "<<std::endl;
    printvector(reverse(v));

    return 0;
}